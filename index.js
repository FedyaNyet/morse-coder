//add the appropriate letters dictionary
var	Transcoder = require('./lib/transcoder.js'),
	Sequencer = require('./lib/sequencer.js')
	Alphabet = require('./lib/alphabet');

var args = process.argv.slice(2);

//Use the transcoder if input isn't Morse Code
var tc = new Transcoder(Alphabet.letters);
var message = tc.encode(args[0]);
var remove1 = tc.encode(args[1]);
var remove2 = "";
if(args.length > 2)
	remove2 = tc.encode(args[2]);

var sequencer = new Sequencer(message);

var start = Date.now();
sequences = sequencer.remove(remove1, remove2);
var end = Date.now();

// console.log((end - start)/60000);
console.log(sequences.length);