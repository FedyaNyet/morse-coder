/*
Give the String class some convenience methods. */
String.prototype.replaceCharAt = function(index, char){
	return this.substr(0, index) + char + this.substr(index+1);
}
String.prototype.removeAllChar = function(char){
	return this.replace(new RegExp(char, "g"), "");
}
String.prototype.charCount = function(char){
   return this.split(char).length - 1;
}
String.prototype.charCount = function(char){
	count = 0;
	for(var c=0; c<this.length; c++){
		if(this.charAt(c)==char)
			count++;
	}
	return count;
}

module.exports = Sequencer;

function Sequencer(message){

	//public
	this.message = message;

	//private
	MATCH_CHAR = "X";

	/**
	 * Itterativly find all the results of remoing one string from another.
	 * @param  {String} message The subject string from which `remove` will be removed
	 * @param  {String} remove  The string to remove from 'message'
	 * @return {array}         array of unique strings resulted from removing `remove` from `message`
	 */
	function itterative_remove(message, remove){

		//dictionary to store unique strings
		var complete = {}; 

		//stack to keep track of itterations, with first itteration supplied.
		var stack = [];
		stack.push([message, remove]);
		while(stack.length){

			//pop the tuple to be worked on.
			var tup = stack.pop();
			var _message = tup[0];
			var _remove = tup[1];

			//if the tuple was completed, remove the x's and store it in the complete dictionary
			if(_remove.charCount(MATCH_CHAR) == _remove.length){
				var cleaned = _message.removeAllChar(MATCH_CHAR);
				complete[cleaned] = 1;
				continue;
			}

			//mark the next matching characters, and push the resulting string for later processing
			var r = _remove.lastIndexOf(MATCH_CHAR)+1;
			for(var m=_message.lastIndexOf(MATCH_CHAR)+1; m<_message.length; m++){
				if(_message.charAt(m) != _remove.charAt(r))
					continue;

				//place an X in the matching character, and store it.
				var _m = _message.replaceCharAt(m,MATCH_CHAR);
				var _r = _remove.replaceCharAt(r, MATCH_CHAR);
				stack.push([_m,_r]);
			}
		}
		return complete;
	}

	/**
	 * (Public) Remove two morse-code strings from the morese-code message attribute.
	 * @param  {String} remove1 First string to be removed from the message
	 * @param  {String} remove2 Second string to be removed fro mthe message
	 * @return {Array}         The unique sequences produces by removed both sequences from the message
	 */
	this.remove = function(remove1, remove2){
		var first_pass = itterative_remove(this.message, remove1);
		//return the remove results if remove2 wasn't supplied.
		if(!remove2)
			return Object.keys(first_pass);
		//build dictionary of unique results of removing `remove2` from every result of removing `remove1` from `message`
		var ret = {};
		for(var first_message in first_pass){
			var second_pass = itterative_remove(first_message, remove2);
			for(var second_message in second_pass){
				ret[second_message] = 1;
			}
		}
		//return the unique strings.
		return Object.keys(ret);
	}

};