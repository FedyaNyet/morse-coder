module.exports = function(alphabet){

	this.alphabet = alphabet;

	this.isMorseCode = function(input){
		for(var i=0; i<input.length; i++){
			var character = input.charAt(i).toLowerCase();
			if(this.alphabet[character])
				return false;
		}
		return true;
	}
	
	this.encode = function(message){
		if(this.isMorseCode(message))
			return message;
		encoded = "";
		for (var x = 0; x < message.length; x++){
			letter = message.charAt(x).toLowerCase();
		    if(x && letter != this.alphabet.SPACE && message.charAt(x-1) != this.alphabet.SPACE){
		    	encoded += "_";
		    }
		    encoded += this.alphabet[letter];
		}
		return encoded;
	}
}