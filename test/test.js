var assert = require('assert'),
	Transcoder = require('../lib/transcoder.js'),
	Sequencer = require('../lib/sequencer.js')
	Alphabet = require('../lib/alphabet');

describe('Transcode', function(){

	it('should encode strings',function(){
		var tc = new Transcoder(Alphabet.letters);
		assert.equal(tc.encode("ABCD"), "*-_-***_-*-*_-**");
		assert.equal(tc.encode("ST"), "***_-");
		assert.equal(tc.encode("ZN"), "--**_-*");
	});

	it('should not encode already encoded strings', function(){
		var tc = new Transcoder(Alphabet.letters);
		assert.equal(tc.encode("*-_-***_-*-*_-**"), "*-_-***_-*-*_-**");
		assert.equal(tc.encode("***_-"), "***_-");
		assert.equal(tc.encode("--**_-*"), "--**_-*");
	});
})

describe('Sequencer',function(){

	it('should remove one string from another', function(){
		var sequencer = new Sequencer("*-_-***");
		sequences = sequencer.remove("*-*");
		assert.equal(sequences.length, 2);
		assert.equal(sequences[1], "_-**");
		assert.equal(sequences[0], "-_**");
	});

	it('should remove two strings from a single other', function(){
		var sequencer = new Sequencer("*-_-***_-*-*_-**");
		sequences = sequencer.remove("***_-", "--**_-*");
		assert.equal(sequences.length, 5);
		assert.equal(sequences[0], '*_-*');
		assert.equal(sequences[1], '*-_*');
		assert.equal(sequences[2], '_*-*');
		assert.equal(sequences[3], '_-**');
		assert.equal(sequences[4], '-_**');
	})
});